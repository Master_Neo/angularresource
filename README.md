# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact



## <?php
/**
  QUESTION 1
  Step 1
  Create a simple HTML table that has 3 rows and 2 columns.
  Step 2
  Into the first column, enter labels for Firstname, Surname and ID Number and in the second column
  put in input fields where a user can enter their Firstname, Surname and ID Number.

  Step 3
  Create a form that will submit this information to the same page

  Step 4
  On the same page, take the submitted information and write a SQL query
  that will insert the posted information into a table called tbl_Person, that has columns
  col_firstname, col_surname, col_idnumber.

  Note: It's optional whether you want to write code that connects to a database and code
  that inserts into the database. We just want to see the SQL query, that uses the posted
  variables to insert into table person
*/




?>
<!-- SUPPLY YOUR ANSWER BELOW THIS COMMENT -->


1. I used laravel 5.6 and angular Js 1 : 
 -  there is no sql you expected to see...
 - I could write you a simple database connection, selection and query submission, but that is an old way of developing.
	However, PHP Laravel made things more safer and easy to do.

 - I dont know if my work has answered your question, I also dont know why you need to see sql.

2. you will need to follow these instructions below to run the app.

	2.1 Install :
	 - XAMPP 7 and above  : Start/run xampp after install, and start apache service and mysql service. 
	 -  Install composer from : https://getcomposer.org/download/ :  
		and specify your php.exe from [c:../xampp/php/php.exe ]folder during installation and finish installation. you may select dev mode.
				
				php artisan serve
				
				- copy url provided : ( http://127.0.0.1:8000 ) and paste it in browser, you should see laravel welcome page.
	 - No need to install node js : node modules are present in the php test zip/ htdocs zip.
	 - open xampp and click on  mysql admin and navigate to myPhpAdmin and create database named  : limabeandev (exactly as instructed ).
	 - open cmd/terminal and type  the following :
	 
	 php artisan make:migration users
	 
	 after navigate to ums extracted from zip to xampp/htdocs/ums open ums folder, open database folder, open migration folder, 
	 edit : the file created via cmd migration migration, and add the code below:
	 
	  	HOW TO RUN THE APP :

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('surname');
            $table->integer('idnumber');			
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Users');
    }
}

	 
	 
	 
	 
 3. open browser and type:

		http://localhost/umsfrontend/#!/signup
		
		
		
4. see the app and add person, via text fields and submit by clicking the Sign Up button in green color...
		- see inserted people by clicking on the next pagination button...(pagination is currently set to two records) 
		
		NOTE : no validation were perfomed... as for ID field to enter any five number and not more, because the database may complain about intergrity violation.
		
		
		The end, thank you for  everytthing.
		
		
		